module.exports = {
    docs: [
        {
            type: 'category',
            label: 'Information',
            items: [
                'faq',
                'rules',
                'ranks',
                'point-system',
                'guild-leadership',
            ],
        },
        {
            type: 'category',
            label: 'Challenges',
            items: [
                'challenges/weekly-challenge',
                'challenges/monthly-challenge',
            ],
        },
        'member-of-the-month',
    ],
};

---
title: Point System
---

![wow-book](/img/wowbook.png)
Achievement members can earn points on their main character to rank up. Points can be earned in many different ways, but all of them are intended to be through means that really help to strengthen our community at its core.

_\*Please note, special circumstances may lower or increase the point value at
the GM, CO-GM, or Officer’s discretion, but this is a baseline for our point
system._

Keep in mind that the GMs and officers of Achievement cannot always be online,
and therefore won’t always be aware of completed tasks. Because of this, if you
see someone doing something nice for someone else, be sure to post about it in
the #refer channel in discord for evaluation!

In the end, points are awarded at the discretion of the GMs and Officers, and
might not be rewarded for each and every single task (especially if it is a task
that has been repeated often in a short time frame by the same person). However,
we encourage you to keep being friendly and doing kind things for others, even
if it doesn’t earn you points. After all, our goal is to make Achievement a
welcoming place to be!

_\*More ways to earn points may be added in the future._

### 5 POINTS

-   ORGANIZING ANY GUILD EVENT WITH AT LEAST 5+ PEOPLE IN ATTENDANCE
-   +3 BONUS POINTS FOR GUILD ACHIEVEMENTS THAT REQUIRE 8+ PEOPLE
-   GIVE FEEDBACK OR SUGGESTIONS THAT ARE APPROVED AND DIRECTLY INFLUENCE THE
    GUILD IN A POSITIVE WAY

### 3 POINTS

-   HELPING COMPLETE A GUILD ACHIEVEMENT
-   ATTENDING GUILD EVENT
-   DONATE TO GUILD BANK OR WEBSITE
    -   _DONATION POINTS CAN ONLY BE EARNED THIS WAY ONCE, SO YOU CANNOT “BUY”
        RANKS._

### 2 POINTS

-   JOIN A GUILD MEMBER IN A CURRENT CONTENT DUNGEON
-   JOIN A GUILD MEMBER IN A DUNGEON OR RAID TO EARN OLD ACHIEVEMENTS
-   HELPING EXPLAIN SOMETHING DIFFICULT TO SOMEONE. (CONVOLUTED QUEST LINE, RAID
    MECHANICS, END GAME STRATEGIES, CLASS/SPEC HELP ETC)

### 1 POINTS

-   RANDOM ACT OF KINDNESS
-   BUMPING THE FORUM RECRUITMENT BOARD WHEN THERE HASN’T BEEN A POST IN OVER 24
    HOURS

---
title: Member of the Month
---

## August

## Trollseeks

#### Character Info

Troll Feral Druid

#### Favorite Mount

I honestly don't care about mounts but my favorite one is the Ultramarine Qiraji
Battle Tank because I didn't play back in Vanilla and that model was one of my
favorites so when they made is available as a mount again for Archeology I
grinded it for quite awhile.

#### Favorite Pet

Argent Gruntling. The Man brings you things. Easy.

#### Proudest Accomplishment

I'm not sure if I really have one, to be honest. I'm not much of a collector or
achievement hunter or anything like that. I'll hit you with the cop out answer
of "It's the friends we made along the way." I haven't been able to keep in
contact with my friends of many years after moving all over the place, aside
from using the internet, and WoW is a game that we have all thoroughly enjoyed
at one point or another and it has allowed me to make new friends as well so I
think being able to keep in touch with the very important people of my life,
meet new friends, and meet the person who is most important to me could be
considered a proudest achievement.

### GM/Officer Comment...

Since you and your better half have joined you have been nothing but awesome in
the guild. You are super helpful with any activity you are a part of. Mynn says
you have to be dragged to do content, but we all know you secretly love helping
out making the guild amazing. You are a staple among guild members and we are
all better for it. Thank you for being with us in our little corner of Azeroth.

_- Galafen_

---
title: Monthly Challenge
---

## August

### Can you find the officer?

#### Objective:

August's Monthly Challenge consists of an Officer scavenger hunt. Each week 4
new clues will be posted. To gain points you must reproduce these clues.

#### Requirements:

Use the weekly clues to make your own version of it.

​

Officer giving clues for the week:

-   Week 1 (1st-6th): Galafen
-   Week 2 (7th-13th): Blox
-   Week 3 (14th-20th): Diamandala
-   Week 4 (21st-27th): Braedy

​

The clues will be posted in the `#august-monthly-clues` each week for study.
Finding the location will be hard, but replicating the image will be easy. You
simply have to find it and take an image. However, that would be boring, so we
would love to see your spin on the image. Add some toys, ride your favorite
mount, or cuddle with your favorite battle pet. After you have your images
upload them to the `#submission-entry` channel individually or use the site
https://imgur.com/ to make a gallery of them.

You can submit them weekly or all at once at the end of the month. We will be
giving the last 4 days of the month (28th-31st) as a grace period to finish up
finding all the locations.

#### Reward:

All 16 locations found and recreated - 25 points
